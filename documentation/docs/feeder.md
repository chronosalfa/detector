# feeder

Main package of the *detector*. Contains all the important sources.

# feeder.feeder

This module is used to gather the feeds from RSS and Atom.

## gatherFeed
```python
gatherFeed(rss_url: str) -> List[Dict[str, Any]]
```

Gathers the feed entries from the RSS or ATOM url and return it as a list of dictionaries.

# feeder.config

Config module. Contains the YAML configuration class for the detector

## DetectorConfig
```python
DetectorConfig(self, yamlPath: str)
```

Detector Configuration object

### loadConfig
```python
DetectorConfig.loadConfig(self, yamlPath: Union[str, NoneType] = None)
```

Loads the configuration from the YAML file.

If the YAML file doesn't exist, it will create default YAML file and end with raised ValueError.

# feeder.lookup

This module is used to search the database for the results.

## lookup
```python
lookup(collection: pymongo.collection.Collection, searchString: str) -> Tuple[str, List[Dict[str, Any]]]
```

Searches the database collection for the full-text search string.

## countResults
```python
countResults(collection: pymongo.collection.Collection, searchString: str) -> int
```

Counts the returned results.

## lookup_range
```python
lookup_range(collection: pymongo.collection.Collection, searchString: str, after_date: datetime.datetime, before_date: datetime.datetime = datetime.datetime(2019, 7, 3, 20, 52, 18, 59564)) -> Tuple[str, List[Dict[str, Any]]]
```

Looks up for the data at a specified range.
Asterisk is allowed if you want to search for all the results in the specified time range.

## lookup_today
```python
lookup_today(collection: pymongo.collection.Collection, searchString: str) -> Tuple[str, List[Dict[str, Any]]]
```

Searches for the results from today. Asterisk is allowed instead of the search string.

## lookup_week
```python
lookup_week(collection: pymongo.collection.Collection, searchString: str) -> Tuple[str, List[Dict[str, Any]]]
```

Searches for the results that are 7 days old at max. Asterisk is allowed instead of the search string.

## text_format
```python
text_format(search_output: Tuple[str, List[Dict[str, Any]]])
```

Formats the output as a markdown document.

## text_format_output
```python
text_format_output(outputFile: str, search_output: Tuple[str, List[Dict[str, Any]]])
```

Formats the output as a markdown document and stores it in a file.

## Connector
```python
Connector(self, hostname: str, port: int, username: str, password: str, authSource: str)
```

Object Connector is used to connect to the MongoDB database

### initCollection
```python
Connector.initCollection(self, database: str, collection: str)
```

Checks if the collection exists. If it doesn't, it will create it and it will also create a couple of indices.

1. Unique index on title, published and source. This is to assure there are only unique documents in the collection.
2. Text index on title, summary and source. This is to allow the full-text search capability.

### disconnect
```python
Connector.disconnect(self)
```

Disconnects from the MongoDB.

# feeder.store

This module is used to store the unique data in the database.

## store_to_database
```python
store_to_database(collection: pymongo.collection.Collection, feed_data: List[Dict[str, Any]])
```

Stores the data in the database collection.

