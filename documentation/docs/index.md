# Detector

This is a documentation for a free open-source tool named Detector.

## Purpose of the tool

This tool was made to be able to gather and store RSS and Atom data in the MongoDB database.

## Requirements

This tool uses `pipenv` to maintain the proper dependency packages, so if you want to use this tool, you have to install pipenv first with

```sh
pip install pipenv
```

Then you can use pipenv to create a separate virtual environment for this project and synchronize the dependencies using the following commands

```sh
pipenv shell
pipenv sync
```

After that you can use the `detector.py` file as the main python script to work with the RSS and the database.

## Freezing the package

It is possible to freeze the package with `pyinstaller`. This will create a binary executable file.

## Installation and setup

When you run the tool for the first time, a configuration file called *detector_cfg.yaml* will be created in the user home in the `.chronos` folder. Open the YAML file and fill in all the required settings to make sure this tool is working properly.

First time you open the YAML configuration file, it looks like this:

```yaml
mongodb:
  collection: col
  database: db
  hostname: localhost
  login:
    authSource: db
    password: pass
    username: user
  port: 27017
rss_urls:
    - url1
    - url2
```

An example of the working configuration is as follows:

```yaml
mongodb:
  hostname: localhost
  port: 27017
  login:
    username: johndoe
    password: secretkey
    authSource: mydb
  database: mydb
  collection: feeds
rss_urls:
  - https://reddit.com/r/news+globalnews+intelligence+espionage+osint+netsec+worldnews.xml
  - http://feeds.feedburner.com/defense-news/global
  - http://feeds.feedburner.com/defense-news/home
  - http://feeds.feedburner.com/defense-news/air
  - http://feeds.feedburner.com/defense-news/land
  - http://feeds.feedburner.com/defense-news/naval
  - http://feeds.feedburner.com/defense-news/space
  - http://feeds.feedburner.com/defense-news/c4isr
  - http://feeds.feedburner.com/defense-news/pentagon
  - http://feeds.feedburner.com/defense-news/congress
  - http://feeds.reuters.com/reuters/topNews
```

## Using the application

If you run this tool on the command line without any arguments, it will return its help menu.

```
usage: detector.py [-h] [-c] [-C] [-s SEARCH] [-o OUTPUTFILE] [-t] [-w]

Detector used to collect and search through information

optional arguments:
  -h, --help            show this help message and exit
  -c, --collect         Collects the news feed and stores it in the database
  -C, --count           Don't show the results, just the number of found
                        entries
  -s SEARCH, --search SEARCH
                        Search through the database with a search string
  -o OUTPUTFILE, --outputFile OUTPUTFILE
                        Store the search result in an output file
  -t, --today           Show results only for today (last 24 hours)
  -w, --week            Show results only for this week
```

From there we see that we can use e.g. `detector.py -c` to collect the data or `detector.py -s "This is my search string" to look through the data.`
