from setuptools import setup, find_packages

with open("requirements.txt", "r", encoding="utf-8") as r:
    requirements = r.readlines()

if __name__ == "__main__":
    setup(name="Detector Feeder",
            description="This project is used to download data from the RSS feeds and store them in the MongoDB",
            version="1.0",
            author="Chronos Alfa",
            packages=find_packages(),
            install_requires=requirements[1:],
            keywords="RSS MongoDB Feeds Store Database",
            entry_points={
                "console_scripts": [
                    "detector = detector_feeder.detector:main"
                ]
            },
            url="https://gitlab.com/chronosalfa/detector"
    )