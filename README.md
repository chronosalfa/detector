# Detector

This ia an open-source tool licensed under the MIT license for obtaining data from the RSS

## Requirements

This project uses `pipenv` for the dependency management. If you do not have this package already, please install it via `pip install pipenv`.

Once you have install pipenv, you can go to the project folder and run following commands there:

```sh
pipenv shell
pipenv sync
```

To activate the virtual environment and install all the required packages there.