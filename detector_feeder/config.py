"""
Config module. Contains the YAML configuration class for the detector
"""
import sys, os
from yaml import load, dump, loader, dumper
from typing import Optional

class DetectorConfig:
    """
    Detector Configuration object
    """
    def __init__(self, yamlPath: str):
        """
        Initializes the detector configuration object.
        """
        self.path = yamlPath

        # default values
        self.mongodb = {}
        self.rss_urls = []
    
    def _checkPathExists(self) -> bool:
        """
        Checks if the path to the YAML file exists.
        """
        return os.path.exists(self.path)

    def loadConfig(self, yamlPath: Optional[str] = None):
        """
        Loads the configuration from the YAML file.

        If the YAML file doesn't exist, it will create default YAML file and end with raised ValueError.
        """
        if yamlPath != None:
            self.path = yamlPath

        if self._checkPathExists():
            with open(self.path) as f:
                output = load(f, Loader=loader.SafeLoader)
                self.__dict__.update(output)
        else:
            with open(self.path, "w") as f:
                dump({"mongodb": {"hostname": "localhost", "port": 27017,
                    "login": {"username": "user", "password": "pass", "authSource": "db"},
                    "database": "db", "collection": "col"}, "rss_urls": ["url1", "url2"]}, f, Dumper=dumper.SafeDumper)
            
            raise ValueError(f"Missing configuration. Created default yaml file at:\n{self.path}\n\nEdit this file before proceeding.")

if __name__ == "__main__":
    cfg = DetectorConfig("../detector_cfg.yaml")
    cfg.loadConfig()

    print(cfg.mongodb["hostname"])