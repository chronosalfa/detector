from argparse import ArgumentParser
from detector_feeder import feeder, store, lookup, config
from detector_feeder.lookup import Connector
import sys, os
from pymongo.collection import Collection

def getConfig()->config.DetectorConfig:
    cfg_name = "detector_cfg.yaml"
    cfg_path = os.path.join(os.path.expanduser("~"), ".chronos")

    os.makedirs(cfg_path, exist_ok=True)

    cfg_file = os.path.join(cfg_path, cfg_name)

    cfg = config.DetectorConfig(cfg_file)
    cfg.loadConfig()

    return cfg

def createArguments()->ArgumentParser:
    arg_parser = ArgumentParser(description="Detector used to collect and search through information")
    arg_parser.add_argument("-c", "--collect", help="Collects the news feed and stores it in the database", action="store_true")
    arg_parser.add_argument("-C", "--count", help="Don't show the results, just the number of found entries", action="store_true")
    arg_parser.add_argument("-s", "--search", help="Search through the database with a search string")
    arg_parser.add_argument("-o", "--outputFile", help="Store the search result in an output file")
    arg_parser.add_argument("-t", "--today", help="Show results only for today (last 24 hours)", action="store_true")
    arg_parser.add_argument("-w", "--week", help="Show results only for this week", action="store_true")

    return arg_parser

def getConnector(cfg: config.DetectorConfig)->lookup.Connector:
    hostname = cfg.mongodb["hostname"]
    port = cfg.mongodb["port"]
    username = cfg.mongodb["login"]["username"]
    password = cfg.mongodb["login"]["password"]
    authSource = cfg.mongodb["login"]["authSource"]
    database = cfg.mongodb["database"]
    collection = cfg.mongodb["collection"]
    connector = lookup.Connector(hostname, port, username, password, authSource)
    connector.initCollection(database, collection)

    return connector

def main():
    try:
        arg_parser = createArguments()

        if len(sys.argv) < 2:
            arg_parser.print_help()
            sys.exit(1)

        args = arg_parser.parse_args()

        if args.week and args.today:
            print("Cannot use both --week and --today parameters. Exiting")
            sys.exit(1)
        
        cfg = getConfig()
        connector: Connector = getConnector(cfg)
        collection = connector.col
        
        if args.collect:
            all_feeds = []

            print("Setting up the list of feeds")

            rss_urls = cfg.rss_urls

            print("Gathering the data from the feeds")

            for rss_url in rss_urls:
                all_feeds.extend(feeder.gatherFeed(rss_url))
            
            print("Storing the unique results")
            store.store_to_database(collection, all_feeds)
            print("Done")

        if args.search:
            if args.count:
                count = lookup.countResults(collection, args.search)
                print(f"Number of found results: {count}")
                sys.exit(0)
            
            if args.today:
                results = lookup.lookup_today(collection, args.search)
            elif args.week:
                results = lookup.lookup_week(collection, args.search)
            else:
                results = lookup.lookup(collection, args.search)
                
            if(args.outputFile):
                lookup.text_format_output(args.outputFile, results)
            else:
                lookup.text_format(results)

        connector.disconnect()
    except Exception as e:
        sys.stderr.write(f"ERROR: {e}\n")

if __name__ == "__main__":
    main()