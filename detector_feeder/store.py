"""
This module is used to store the unique data in the database.
"""
from typing import List, Dict, Any
from pymongo import MongoClient
from pymongo.collection import Collection

def store_to_database(collection: Collection, feed_data: List[Dict[str, Any]]):
    """
    Stores the data in the database collection.
    """
    col = collection
    counter = 0
    
    for feed in feed_data:
        try:
            col.insert_one(feed)
            counter+=1
        except:
            pass
    
    print(f"Collection finished. Found {counter} new entries.")
    