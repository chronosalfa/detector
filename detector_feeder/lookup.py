"""
This module is used to search the database for the results.
"""
from pymongo import MongoClient
from pymongo.database import Database
from pymongo.collection import Collection
from pymongo import IndexModel, ASCENDING
from typing import List, Dict, Any, Tuple
from datetime import datetime, timedelta
import html2markdown

def lookup(collection: Collection, searchString: str)->Tuple[str, List[Dict[str, Any]]]:
    """
    Searches the database collection for the full-text search string.
    """
    col = collection
    output = [item for item in col.find({"$text": {"$search": searchString}})]

    return (searchString, output)

def countResults(collection: Collection, searchString: str) -> int:
    """
    Counts the returned results.
    """
    col = collection
    count = 0

    if searchString != "*":
        count = col.count_documents({"$text": {"$search": searchString}})
    else:
        count = col.count_documents({})

    return count

def lookup_range(collection: Collection, searchString: str, after_date: datetime, before_date: datetime = datetime.utcnow())->Tuple[str, List[Dict[str, Any]]]:
    """
    Looks up for the data at a specified range.
    Asterisk is allowed if you want to search for all the results in the specified time range.
    """
    query = {"$and": [{"published": {"$lte": before_date}}, {"published": {"$gte": after_date}}]}
    if searchString != "*":
        query["$text"] = {"$search": searchString}
    output = [item for item in collection.find(query)]

    return (searchString, output)

def lookup_today(collection: Collection, searchString: str)->Tuple[str, List[Dict[str, Any]]]:
    """
    Searches for the results from today. Asterisk is allowed instead of the search string.
    """
    yesterday = datetime.utcnow() - timedelta(days=1)

    return lookup_range(collection, searchString, yesterday)

def lookup_week(collection: Collection, searchString: str)->Tuple[str, List[Dict[str, Any]]]:
    """
    Searches for the results that are 7 days old at max. Asterisk is allowed instead of the search string.
    """
    last_week = datetime.utcnow() - timedelta(days=7)

    return lookup_range(collection, searchString, last_week)

def text_format(search_output: Tuple[str, List[Dict[str, Any]]]):
    """
    Formats the output as a markdown document.
    """
    search_input, search_results = search_output
    for entry in search_results:
        assert "title" in entry.keys()
        assert "summary" in entry.keys()
        assert "published" in entry.keys()
        assert "source" in entry.keys()

    print(f"# Searching for '{search_input}'")
    for entry in search_results:
        print(f"## {entry['title']}\n")
        print(f"Date: *{entry['published']}*\n")
        print(html2markdown.convert(entry['summary']))
        print("")
        print(f"Source: {entry['source']}\n")
    print(f"Search complete, found results: *{len(search_results)}*")

def text_format_output(outputFile: str, search_output: Tuple[str, List[Dict[str, Any]]]):
    """
    Formats the output as a markdown document and stores it in a file.
    """
    search_input, search_results = search_output
    for entry in search_results:
        assert "title" in entry.keys()
        assert "summary" in entry.keys()
        assert "published" in entry.keys()
        assert "source" in entry.keys()

    with open(outputFile, "w") as f:
        f.write(f"# Searching for '{search_input}'")
        for entry in search_results:
            f.write(f"## {entry['title']}\n")
            f.write(f"Date: *{entry['published']}*\n")
            f.write(html2markdown.convert(entry['summary']))
            #print(entry["summary"])
            f.write("")
            f.write(f"Source: {entry['source']}\n")
        f.write(f"Search complete, found results: *{len(search_results)}*")

class Connector:
    """
    Object Connector is used to connect to the MongoDB database
    """
    def __init__(self, hostname: str, port: int, username: str, password: str, authSource: str):
        """
        Initializes the connection to the MongoDB database
        """
        self.hostname = hostname
        self.port = port
        self.username = username
        self.password = password
        self.authSource = authSource
        self.__connect()
    
    def __connect(self):
        """
        Connects to the database. This is an internal method and should not be called from outside of the class.
        """
        self.mc = MongoClient(self.hostname, self.port, username=self.username, password=self.password, authSource=self.authSource)
    
    def initCollection(self, database: str, collection: str):
        """
        Checks if the collection exists. If it doesn't, it will create it and it will also create a couple of indices.

        1. Unique index on title, published and source. This is to assure there are only unique documents in the collection.
        2. Text index on title, summary and source. This is to allow the full-text search capability.
        """
        db: Database = self.mc[database]
        if collection not in db.list_collection_names():
            col: Collection = db[collection]
            col.create_indexes([
                IndexModel([("title", 1),("published", 1),
                        ("source", 1)], unique=True),
                IndexModel([("title", "text"),
                        ("summary", "text"),
                        ("source", "text")])
            ])
        self.col: Collection = db[collection]

    def disconnect(self):
        """
        Disconnects from the MongoDB.
        """
        self.mc.close()