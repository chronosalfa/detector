"""
This module is used to gather the feeds from RSS and Atom.
"""
import feedparser
from datetime import datetime
from time import mktime
from typing import Dict, List, Any

def gatherFeed(rss_url: str) -> List[Dict[str, Any]]:
    """
    Gathers the feed entries from the RSS or ATOM url and return it as a list of dictionaries.
    """
    feed = feedparser.parse(rss_url)
    data = []

    for entry in feed["entries"]:
        struct_date = entry["published_parsed"] if entry.has_key("published_parsed") else entry["updated_parsed"]
        actual_date = datetime.utcfromtimestamp(mktime(struct_date))
        summary = entry["summary"] if entry.has_key("summary") else entry["description"]

        parsed_entry = {
            "title": entry["title"],
            "summary": summary,
            "source": entry["link"],
            "published": actual_date
        }

        data.append(parsed_entry)
    
    return data